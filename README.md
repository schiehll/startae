# startaê test

> See the demo running [here](http://startae-test.now.sh/)

## How to install

I've used `yarn` to build it, and the scripts in the `package.json` file assume that you are trying to run it in a UNIX system. If you are on a Windows machine, it will probrably not run given the way that the envainroment variables where setted.

Also, it was tested with node v10.7.0.

That being said, to install the dependencies you need to have `yarn` installed to run `yarn install`.

## How to run the dev build

Running `yarn dev` you should be able to access the dev build in the port 3000.

## How to run the prod build

This one require one more step. First, you have to run `yarn build`, wait for the build proccess to complete, then run `yarn start`. After that you should be able to access the prod build in the port 3000.

## How to run the tests

I've created some e2e tests with cypress. You can run it with `yarn test`, but you will need to have either, the `dev` or the `prod` build running on port 3000 before running the tests.

## Project structure

I've implemented a small GraphQL server that only return the JSON with the team members. The code for the server lives in the `src/server` folder.

_You can access the GraphQL playground in the url `localhost:3000/playground` while running the dev build_

The frontend code that consumes the JSON via GraphQL lives in the `src/client` folder.

Inside the `client` folder there's the following structure:

```
client folder
├── components // here whe have all components that aren't screens
├── fonts // the project font files
├── graphql // the GraphQL client and the single query we have
├── images // contains only the favicon
├── routes // the route file
├── screens // those are components that represents a screen
├── styles // here we have the variables used in the project, as well as some style related utils
└── utils // project wide utils
```

## Frontend Tech Stack

- React
- Styled Components
- Reach Router
- React Apollo
- Loadable Components (for code-splitting)
- Webpack
- Babel 7
