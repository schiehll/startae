import team from '../../src/server/data/team'

describe('timeline', () => {
  it('should load tweets from the given user', () => {
    cy.visit('/')

    cy.get('li')
      .first()
      .click()

    cy.location().then(loc => {
      cy.get('p[data-test="tweet"]')
        .first()
        .then($tweet => {
          expect($tweet.text()).to.eq(`"${team[0].tweet}"`)
        })
    })
  })

  it('should go back to home on logo click', () => {
    cy.visit('/')

    cy.get('li')
      .first()
      .click()

    cy.location().then(loc => {
      expect(loc.pathname).to.contains('/timeline/')
      cy.get('svg[data-test="text-logo"]').click()
      cy.location().then(loc => {
        expect(loc.pathname).to.eq('/')
      })
    })
  })
})
