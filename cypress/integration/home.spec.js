import team from '../../src/server/data/team'

describe('home', () => {
  it('should load team members and list them in the page', () => {
    cy.visit('/')

    cy.get('li').should('have.length', team.length)
  })

  it('should remove the text part of the logo on mobile', () => {
    cy.visit('/')

    cy.viewport(1500, 800)
    cy.get('svg[data-test="text-logo"]').should('be.visible')

    cy.viewport(900, 800)
    cy.get('svg[data-test="text-logo"]').should('not.be.visible')
  })

  it('should go to the timeline screen on team member click', () => {
    cy.visit('/')

    cy.get('li')
      .first()
      .click()

    cy.location().should(loc => {
      expect(loc.pathname).to.contains('/timeline/')
    })
  })
})
