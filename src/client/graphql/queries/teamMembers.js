import gql from 'graphql-tag'

export default gql`
  query TeamMembers {
    teamMembers {
      id
      name
      avatar
      role
      twitter
      tweets
    }
  }
`
