import React from 'react'
import Spinner from 'client/components/spinner'

import * as S from './styles'

const PageLoader = () => (
  <S.Wrapper>
    <Spinner />
  </S.Wrapper>
)

export default PageLoader
