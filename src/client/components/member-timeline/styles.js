import styled, { css } from 'styled-components'
import media from 'client/styles/utils/media'

export const Title = styled.h2`
  ${({ theme: { colors, font, spacing } }) => css`
    display: flex;
    width: 100%;
    align-items: center;
    justify-content: center;
    color: ${colors.white};
    font-weight: ${font.weights.regular};
    margin-top: ${spacing.huge * 2}px;

    ${media.lessThan('desktop')`
      margin-top: ${spacing.huge}px;
      margin-bottom: ${spacing.small}px;
      justify-content: flex-start;
    `} > svg {
      width: 20px;
      height: 20px;
      margin-right: ${spacing.base}px;
    }
  `};
`

export const Wrapper = styled.div`
  position: relative;
  width: 930px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;

  ${media.lessThan('desktop')`width: 100%`};
`

export const Timeline = styled.ul`
  ${({ theme: { spacing, colors } }) => css`
    width: 100%;
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    list-style: none;
    padding: 0;

    ${media.lessThan('desktop')`
      padding-left: ${spacing.medium}px

      &:before {
        content: '';
        position: absolute;
        width: ${spacing.base / 2}px;
        height: 100%;
        background-color: ${colors.white};
        top: 0;
        left: ${spacing.base / 2}px;
        border-radius: ${spacing.base}px;
      }
    `};

    ${media.greaterThan('desktop')`
      &:before {
        content: '';
        position: absolute;
        width: ${spacing.base / 2}px;
        height: 100%;
        background-color: ${colors.white};
        top: 0;
        left: 50%;
        border-radius: ${spacing.base}px;
      }
    `};
  `};
`

const Arrow = ({ colors, spacing, inverted }) => css`
  &:before {
    content: '';
    width: 0;
    height: 0;
    border-top: ${spacing.base}px solid transparent;
    border-bottom: ${spacing.base}px solid transparent;
    ${inverted
      ? `border-left: ${spacing.base}px solid ${colors.white}`
      : `border-right: ${spacing.base}px solid ${colors.white}`};
    position: absolute;
    left: ${inverted ? '100%' : `-${spacing.base}px`};
    top: calc(50% - ${spacing.base}px);
  }
`

const Dot = ({ colors, spacing, inverted }) => css`
  &:after {
    content: '';
    width: ${spacing.base}px;
    height: ${spacing.base}px;
    display: inline-flex;
    border-radius: 50%;
    background-color: ${colors.white};
    border: 3px solid ${colors.secondary};
    position: absolute;
    left: ${inverted
      ? `calc(100% + ${spacing.small}px)`
      : `-${spacing.medium}px`};
    top: calc(50% - ${spacing.base}px);
  }
`

export const Tweet = styled.li`
  ${({ theme: { colors, spacing, radius } }) => css`
    position: relative;
    background-color: ${colors.white};
    padding: ${spacing.base}px;
    border-radius: ${radius}px;
    display: flex;

    ${media.lessThan('desktop')`
      width: 304px;
      height: auto;
      align-self: flex-start;
      ${Arrow({ colors, spacing, inverted: false })};
      ${Dot({ colors, spacing, inverted: false })};
      margin-bottom: ${spacing.medium}px;
      padding: ${spacing.small}px ${spacing.base}px;
    `} ${media.greaterThan('desktop')`
      width: 440px;
      height: 160px;
      &:nth-child(even) {
        align-self: flex-start;
        ${Arrow({ colors, spacing, inverted: true })};
        ${Dot({ colors, spacing, inverted: true })};
      }

      &:nth-child(odd) {
        align-self: flex-end;
        ${Arrow({ colors, spacing, inverted: false })};
        ${Dot({ colors, spacing, inverted: false })};
      }
    `};
  `};
`

export const AvatarWrapper = styled.div`
  ${({ theme: { spacing } }) => css`
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    width: ${60 + spacing.small}px;
    align-items: center;
    justify-content: center;
    margin-right: ${spacing.base}px;

    ${media.lessThan('desktop')`
      ${Heart} {
        display: none;
      }
    `};
  `};
`

export const Avatar = styled.div`
  ${({ theme: { spacing }, url }) => css`
    background: url(${url}) no-repeat;
    background-position: top;
    background-size: cover;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    margin-bottom: ${spacing.base}px;
  `};
`

export const Heart = styled.div`
  ${({ theme: { colors, spacing } }) => css`
    color: ${colors.gray.light};
    display: flex;
    align-items: center;
    justify-content: center;

    > svg {
      margin-right: ${spacing.base / 2}px;
    }
  `};
`

export const TextWrapper = styled.div`
  ${({ theme: { font } }) => css`
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    width: 100%;
    align-items: flex-start;
    justify-content: center;
    font-size: ${font.sizes.extraSmall}px;

    ${media.lessThan('desktop')`
      flex-direction: column-reverse;
    `};
  `};
`

export const TimeWrapper = styled.div`
  ${({ theme: { colors } }) => css`
    ${media.lessThan('desktop')`
      display: flex;
      width: 90%;
      align-items: center;
      justify-content: space-between;
      color: ${colors.gray.light};
    `};

    ${media.greaterThan('desktop')`
      ${Heart} {
        display: none;
      }
    `};
  `};
`

export const Text = styled.p`
  ${({ theme: { font, colors, spacing } }) => css`
    font-family: ProximaNova-regular-italic, Arial;
    font-style: italic;
    font-size: ${font.sizes.default}px;
    color: ${colors.gray.dark};
    margin: ${spacing.base / 2}px 0 0 0;
    max-width: 90%;

    ${media.lessThan('desktop')`margin: 0 0 ${spacing.base}px 0;`};
  `};
`
