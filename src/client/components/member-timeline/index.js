import React from 'react'
import PropTypes from 'prop-types'
import TwitterLogo from 'client/components/icons/Twitter'
import HeartIcon from 'client/components/icons/Heart'
import getTwitterHandler from 'client/utils/getTwitterHandler'

import * as S from './styles'

const MemberTimeline = ({ twitter, avatar, tweets }) => (
  <main>
    <S.Title>
      <TwitterLogo />@{getTwitterHandler(twitter)}
    </S.Title>
    <S.Wrapper>
      <S.Timeline>
        {tweets.map((tweet, index) => (
          <S.Tweet key={index}>
            <S.AvatarWrapper>
              <S.Avatar url={avatar} />
              <S.Heart>
                <HeartIcon />1
              </S.Heart>
            </S.AvatarWrapper>
            <S.TextWrapper>
              <S.TimeWrapper>
                <S.Heart>
                  <HeartIcon />1
                </S.Heart>
                5h ago
              </S.TimeWrapper>
              <S.Text data-test="tweet">"{tweet}"</S.Text>
            </S.TextWrapper>
          </S.Tweet>
        ))}
      </S.Timeline>
    </S.Wrapper>
  </main>
)

MemberTimeline.propTypes = {
  twitter: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  tweets: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default MemberTimeline
