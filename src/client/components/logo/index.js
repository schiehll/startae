import React from 'react'
import Startae from 'client/components/icons/Startae'
import StartaeText from 'client/components/icons/StartaeText'
import { PATHS } from 'client/routes'

import * as S from './styles'

const Logo = () => (
  <S.Link to={PATHS.HOME}>
    <S.Wrapper>
      <Startae />
      <StartaeText />
    </S.Wrapper>
  </S.Link>
)

export default Logo
