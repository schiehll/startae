import styled, { css } from 'styled-components'
import { Link as RouteLink } from '@reach/router'
import media from 'client/styles/utils/media'

export const Wrapper = styled.div`
  ${({ theme: { colors } }) => css`
    display: flex;
    justify-content: space-between;
    align-items: baseline;
    color: ${colors.white};
    height: 40px;
    width: 190px;

    ${media.lessThan('desktop')`
      width: 31px;

      > svg:last-child {
        display: none;
      }
    `};
  `};
`

export const Link = styled(RouteLink)`
  text-decoration: none;
  display: inline-flex;
  align-items: center;
`
