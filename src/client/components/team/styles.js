import styled, { css } from 'styled-components'
import media from 'client/styles/utils/media'

export const Title = styled.h2`
  ${({ theme: { colors, font, spacing } }) => css`
    display: flex;
    width: 100%;
    align-items: center;
    justify-content: flex-start;
    color: ${colors.white};
    font-weight: ${font.weights.regular};
    margin-top: ${spacing.huge * 2}px;
    ${media.lessThan('desktop')`margin-top: ${spacing.huge}px`};

    > svg {
      width: 20px;
      height: 20px;
      margin-right: ${spacing.base}px;
    }
  `};
`

export const Members = styled.ul`
  ${({ theme: { spacing } }) => css`
    list-style: none;
    padding: 0;
    display: flex;
    justify-content: flex-start;
    flex-wrap: wrap;
    margin-bottom: ${spacing.huge}px;
    margin-left: -${spacing.base}px;
    width: calc(100% + ${spacing.small}px);
  `};
`
