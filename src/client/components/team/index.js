import React from 'react'
import { graphql } from 'react-apollo'
import Container from 'client/components/container'
import TwitterLogo from 'client/components/icons/Twitter'
import TeamMember from 'client/components/team-member'
import teamMembersQuery from 'client/graphql/queries/teamMembers'

import * as S from './styles'

const Team = ({ data: { teamMembers } }) => (
  <Container>
    <main>
      <S.Title>
        <TwitterLogo />
        Latest tweets from our team
      </S.Title>
      <S.Members>
        {teamMembers &&
          teamMembers.map(member => <TeamMember key={member.id} {...member} />)}
      </S.Members>
    </main>
  </Container>
)

export default graphql(teamMembersQuery)(Team)
