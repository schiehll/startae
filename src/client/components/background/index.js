import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import GlobalStyles from 'client/styles/global'

import * as S from './styles'

const Background = ({ height }) => (
  <Fragment>
    <S.Background height={height}>
      <S.Separator height={height} />
    </S.Background>
    <GlobalStyles />
  </Fragment>
)

Background.propTypes = {
  height: PropTypes.number
}

Background.defaultProps = {
  height: 550
}

export default Background
