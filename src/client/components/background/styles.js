import styled, { css } from 'styled-components'
import media from 'client/styles/utils/media'

export const Background = styled.div`
  ${({ theme: { colors }, height }) => css`
    position: absolute;
    top: 0;
    left: 0;
    width: 100vw;
    height: ${height}px;
    background-color: ${colors.primary};
    z-index: -1;
  `};
`

export const Separator = styled.div`
  ${({ theme: { colors }, height }) => css`
    position: absolute;
    top: ${height}px;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 500px 100vw 0 0;
    ${media.lessThan('desktop')`border-width: 300px 100vw 0 0`};
    border-color: ${colors.primary} ${colors.secondary} transparent transparent;
    transform: rotateY(180deg);
    z-index: -1;
  `};
`
