import React from 'react'
import PropTypes from 'prop-types'
import { navigate } from '@reach/router'
import HeartIcon from 'client/components/icons/Heart'
import { PATHS } from 'client/routes'
import getTwitterHandler from 'client/utils/getTwitterHandler'

import * as S from './styles'

const TeamMember = ({ id, name, avatar, twitter, role, tweets }) => (
  <S.Card onClick={() => navigate(`${PATHS.TIMELINE.replace(':id', id)}`)}>
    <S.Avatar url={avatar} />
    <S.Info>
      <S.Name>{name}</S.Name>
      <S.TwitterAndRolwWrapper>
        <S.Twitter>@{getTwitterHandler(twitter)}</S.Twitter>
        <S.Role>{role}</S.Role>
      </S.TwitterAndRolwWrapper>
      <S.Tweet>"{tweets[0]}"</S.Tweet>
      <S.Heart>
        <HeartIcon />1
      </S.Heart>
    </S.Info>
  </S.Card>
)

TeamMember.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  twitter: PropTypes.string.isRequired,
  role: PropTypes.string.isRequired,
  tweets: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default TeamMember
