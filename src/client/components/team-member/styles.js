import styled, { css } from 'styled-components'
import media from 'client/styles/utils/media'

export const Card = styled.li`
  ${({ theme: { colors, spacing, radius } }) => css`
    position: relative;
    top: 0px;
    width: 314px;
    background-color: ${colors.white};
    border-radius: ${radius}px;
    margin: 0 ${spacing.base}px ${spacing.small}px ${spacing.base}px;
    transition: top 0.3s ease;

    ${media.lessThan('desktop')`
      width: 166px;
      margin: 0 ${spacing.base / 2}px ${spacing.base}px ${spacing.base / 2}px;
    `};

    &:hover {
      cursor: pointer;
      top: -${spacing.base}px;
      box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.3);
    }
  `};
`

export const Avatar = styled.div`
  ${({ theme: { spacing, radius }, url }) => css`
    background: url(${url}) no-repeat;
    background-position: top;
    background-size: cover;
    border-radius: ${radius}px;
    width: calc(100% - ${spacing.big}px);
    height: 214px;
    margin: ${spacing.small}px;

    ${media.lessThan('desktop')`
      width: 100%;
      height: 145px;
      border-radius: ${radius}px ${radius}px 0 0;
      margin: 0 0 ${spacing.small}px 0;
    `};
  `};
`

export const Info = styled.div`
  ${({ theme: { font, spacing } }) => css`
    display: flex;
    flex-direction: column;
    text-align: center;
    padding: 0 ${spacing.small}px ${spacing.small}px ${spacing.small}px;

    > div:not(:first-child) {
      margin-top: ${spacing.base / 2}px;
    }
  `};
`

export const Name = styled.div`
  ${({ theme: { font } }) => css`
    font-size: ${font.sizes.big}px;
    ${media.lessThan('desktop')`font-size: ${font.sizes.small}px;`};
  `};
`

export const TwitterAndRolwWrapper = styled.div`
  display: flex;
  flex-direction: column;
  ${media.lessThan('desktop')`flex-direction: column-reverse;`};
`

export const Twitter = styled.div`
  ${({ theme: { font } }) => css`
    font-size: ${font.sizes.small}px;
    ${media.lessThan('desktop')`font-size: ${font.sizes.extraSmall}px;`};
  `};
`

export const Role = styled.div`
  ${({ theme: { font, spacing } }) => css`
    font-family: ProximaNova-semibold, Arial;
    font-size: ${font.sizes.small}px;
    margin: ${spacing.base / 2}px 0 0 0;

    ${media.lessThan('desktop')`
      margin: 0 0 ${spacing.base / 2}px 0;
      font-family: ProximaNova-regular, Arial;
      font-size: ${font.sizes.extraSmall}px;
    `};
  `};
`

export const Tweet = styled.p`
  ${({ theme: { colors, font } }) => css`
    font-family: ProximaNova-regular-italic, Arial;
    font-style: italic;
    color: ${colors.gray.dark};
    height: 100px;

    ${media.lessThan('desktop')`
      height: 145px;
      font-size: ${font.sizes.extraSmall}px;
    `};
  `};
`

export const Heart = styled.div`
  ${({ theme: { colors, spacing } }) => css`
    color: ${colors.gray.light};
    display: flex;
    align-items: center;
    position: absolute;
    bottom: ${spacing.small}px;
    right: ${spacing.small}px;

    ${media.lessThan('desktop')`
      right: calc(50% - ${spacing.base}px);
    `};

    > svg {
      margin-right: ${spacing.base / 2}px;
    }
  `};
`
