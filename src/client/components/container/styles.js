import styled, { css } from 'styled-components'
import media from 'client/styles/utils/media'

export const Container = styled.div`
  ${({ theme: { spacing } }) => css`
    margin: 0 auto;
    padding: 0 ${spacing.small}px;
    width: 100%;
    ${media.greaterThan('desktop')`
      padding: 0 ${spacing.big}px;
      max-width: ${1316 + spacing.big * 2}px
    `};
  `};
`
