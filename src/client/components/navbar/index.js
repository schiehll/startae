import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import Background from 'client/components/background'
import Container from 'client/components/container'
import Logo from 'client/components/logo'

import * as S from './styles'

const Navbar = ({ bgHeight }) => (
  <Fragment>
    <Background height={bgHeight} />
    <S.Wrapper>
      <Container>
        <Logo />
      </Container>
    </S.Wrapper>
  </Fragment>
)

Navbar.propTypes = {
  bgHeight: PropTypes.number
}

export default Navbar
