import styled, { css } from 'styled-components'
import media from 'client/styles/utils/media'
import Container from 'client/components/container'

export const Wrapper = styled.div`
  ${({ theme: { spacing } }) => css`
    padding: ${spacing.medium}px 0;
    ${media.lessThan('desktop')`
      width: 100%;
      background-color: rgba(0,0,0,.1);
      box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.2);

      ${Container} {
        display: flex;
        align-items: center;
        justify-content: center;
      }
    `};
  `};
`
