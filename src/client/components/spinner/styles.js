import styled from 'styled-components'
import spin from 'client/styles/animations/spin'

export const Spinner = styled.div`
  border-radius: 50%;
  width: 20px;
  height: 20px;

  border-top: 2px solid currentColor;
  border-right: 2px solid currentColor;
  border-bottom: 2px solid currentColor;
  border-left: 2px solid rgba(0, 0, 0, 0.2);
  animation: ${spin} 0.7s infinite linear;
`
