const getTwitterHandler = twitter => twitter.match(/twitter.com\/(.*)/)[1]

export default getTwitterHandler
