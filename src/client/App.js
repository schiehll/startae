import React, { Component, Fragment } from 'react'
import { ApolloProvider } from 'react-apollo'
import { ThemeProvider } from 'styled-components'
import client from 'client/graphql/client'
import theme from 'client/styles/theme'
import Routes from 'client/routes'

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <ThemeProvider theme={theme}>
          <Fragment>
            <Routes />
          </Fragment>
        </ThemeProvider>
      </ApolloProvider>
    )
  }
}

export default App
