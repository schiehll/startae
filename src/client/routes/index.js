import React, { Component } from 'react'
import { Router } from '@reach/router'
import ScreenRoute from 'client/utils/ScreenRoute'

export const PATHS = {
  HOME: '/',
  TIMELINE: '/timeline/:id'
}

class Routes extends Component {
  render() {
    return (
      <Router>
        <ScreenRoute
          key="home"
          screen="home"
          title="Startaê"
          path={PATHS.HOME}
        />
        <ScreenRoute
          key="timeline"
          screen="timeline"
          title="Startaê - Timeline"
          path={PATHS.TIMELINE}
        />
        <ScreenRoute
          key="no-match"
          screen="no-match"
          title="Startaê - No match"
          default
        />
      </Router>
    )
  }
}

export default Routes
