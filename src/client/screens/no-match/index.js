import React, { Fragment } from 'react'
import Container from 'client/components/container'
import Navbar from 'client/components/navbar'

import * as S from './styles'

const NoMatch = () => (
  <Fragment>
    <Navbar />
    <Container>
      <S.Wrapper>Nothing to see here, go home</S.Wrapper>
    </Container>
  </Fragment>
)

export default NoMatch
