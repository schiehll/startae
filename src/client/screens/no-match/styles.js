import styled, { css } from 'styled-components'

export const Wrapper = styled.header`
  ${({ theme: { font, colors } }) => css`
    font-size: ${font.sizes.big}px;
    color: ${colors.white};
  `};
`
