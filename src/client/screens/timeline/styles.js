import styled, { css } from 'styled-components'
import media from 'client/styles/utils/media'

export const Wrapper = styled.header`
  ${({ theme: { spacing, colors } }) => css`
    margin: ${spacing.huge}px 0;
    color: ${colors.white};
    text-align: left;
    ${media.greaterThan('desktop')`text-align: center`};
    ${media.lessThan('desktop')`
      > h1 {
        margin-bottom: 0;
      }
    `};
  `};
`

export const Role = styled.p`
  ${({ theme: { font } }) => css`
    font-size: ${font.sizes.big}px;
    ${media.lessThan('desktop')`font-size: ${font.sizes.default}px`};
  `};
`
