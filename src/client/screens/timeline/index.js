import React, { Fragment } from 'react'
import { graphql } from 'react-apollo'
import Container from 'client/components/container'
import Navbar from 'client/components/navbar'
import MemberTimeline from 'client/components/member-timeline'
import teamMembersQuery from 'client/graphql/queries/teamMembers'

import * as S from './styles'

const Timeline = ({ data: { teamMembers }, id }) => {
  const member = teamMembers.find(member => member.id === id)

  return (
    <Fragment>
      <Navbar bgHeight={1000} />
      <Container>
        <S.Wrapper>
          <h1>{member.name}</h1>
          <S.Role>{member.role}</S.Role>
        </S.Wrapper>
        <MemberTimeline {...member} />
      </Container>
    </Fragment>
  )
}

export default graphql(teamMembersQuery)(Timeline)
