import React, { Fragment } from 'react'
import Container from 'client/components/container'
import Navbar from 'client/components/navbar'
import Team from 'client/components/team'

import * as S from './styles'

const Home = () => (
  <Fragment>
    <Navbar />
    <Container>
      <S.Wrapper>
        <h1>Meet our team</h1>
        <S.Description>
          We are a group of multi-skilled individuals who are entrepreneurial by
          nature and live to make digital products and services that people love
          to use.
        </S.Description>
      </S.Wrapper>
    </Container>
    <Team />
  </Fragment>
)

export default Home
