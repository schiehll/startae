import styled, { css } from 'styled-components'
import media from 'client/styles/utils/media'

export const Wrapper = styled.header`
  ${({ theme: { spacing, colors } }) => css`
    margin: ${spacing.huge}px 0;
    color: ${colors.white};
  `};
`

export const Description = styled.p`
  ${({ theme: { font } }) => css`
    font-size: ${font.sizes.big}px;
    max-width: 700px;
    ${media.lessThan('desktop')`font-size: ${font.sizes.default}px`};
  `};
`
