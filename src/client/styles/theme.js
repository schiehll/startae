const baseSpacing = 10

const theme = {
  colors: {
    white: '#FFFFFF',
    black: '#000000',
    gray: {
      dark: '#656565',
      light: '#A7B8C3'
    },
    primary: '#5D1EB2',
    secondary: '#FF223E'
  },

  spacing: {
    base: baseSpacing,
    small: baseSpacing * 2,
    medium: baseSpacing * 3,
    big: baseSpacing * 4,
    huge: baseSpacing * 5
  },

  radius: 5,

  font: {
    family: 'ProximaNova-regular',
    weights: {
      regular: 400,
      semibold: 600,
      extrabold: 800
    },
    sizes: {
      default: 16,
      extraSmall: 12,
      small: 14,
      big: 20,
      h1: {
        desktop: 100,
        mobile: 42
      },
      h2: 24
    }
  },

  breakpoints: {
    desktop: 1000,
    mobile: 768
  }
}

export default theme
