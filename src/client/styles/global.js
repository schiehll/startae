import 'client/fonts/ProximaNova-regular.otf'
import 'client/fonts/ProximaNova-regular-italic.otf'
import 'client/fonts/ProximaNova-extrabold.otf'
import 'client/fonts/ProximaNova-semibold.otf'
import { createGlobalStyle, css } from 'styled-components'
import media from 'client/styles/utils/media'

const GlobalStyles = createGlobalStyle`${({
  theme: { font, colors, spacing }
}) => css`
  ${['extrabold', 'regular', 'regular-italic', 'semibold'].map(weight => {
    return `
    @font-face {
      font-family: 'ProximaNova-${weight}', Arial;
      src: url('../fonts/ProximaNova-${weight}.otf');
      font-weight: ${font.weights[weight.split('-')[0]]};
      font-style: ${weight.endsWith('italic') ? 'italic' : 'normal'};
    }
  `
  })};

  body {
    font-family: ${font.family}, Arial;
    font-size: ${font.sizes.default}px;
    font-weight: ${font.weights.regular};
    margin: 0;
    padding: 0;
    color: ${colors.black};
    background-color: ${colors.secondary};
    overflow-x: hidden;
  }

  * {
    box-sizing: border-box;
  }

  h1 {
    font-family: 'ProximaNova-extrabold', Arial;
    font-size: ${font.sizes.h1.desktop}px;
    ${media.lessThan('desktop')`font-size: ${font.sizes.h1.mobile}px`};
    margin: ${spacing.big}px 0 ${spacing.medium}px 0;
  }

  h2 {
    font-family: 'ProximaNova-regular', Arial;
    font-size: ${font.sizes.h2}px;
    ${media.lessThan('desktop')`font-size: ${font.sizes.default}px`};
    margin: ${spacing.big}px 0;
  }
`}`

export default GlobalStyles
