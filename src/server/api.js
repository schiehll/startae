import express from 'express'
import bodyParser from 'body-parser'
import { makeExecutableSchema } from 'apollo-server-express'
import { graphqlExpress } from 'apollo-server-express/dist/expressApollo'
import expressPlayground from 'graphql-playground-middleware-express'
import typeDefs from './graphql/schema'
import resolvers from './graphql/resolvers'

const executableSchema = makeExecutableSchema({ typeDefs, resolvers })

const app = express()

app.use(
  '/api',
  bodyParser.json(),
  graphqlExpress(req => ({ schema: executableSchema }))
)

if (process.env.NODE_ENV !== 'production') {
  app.get('/playground', expressPlayground({ endpoint: '/api/v1' }), () => {})
}

export default app
