import shortid from 'shortid'

export default [
  {
    id: shortid.generate(),
    name: 'Flavio Ludgero',
    avatar:
      'https://startae.com/assets/images/team/flavio-ludgero-normal_2x-2cfa44b7.jpg',
    role: 'Founder & CEO',
    tweet:
      'Service designer and longboard skate lover. Been through business consulting and digital agencies. Work with mentoring and business modeling using lenses of design.',
    twitter: 'https://twitter.com/flavioludgero'
  },
  {
    id: shortid.generate(),
    name: 'Rafael Torales',
    avatar:
      'https://startae.com/assets/images/team/rafael-torales-normal_2x-84e7f1be.jpg',
    role: 'Founder & Product Strategy',
    tweet:
      'As a Product Designer, he is always seeking to apply concept in his work and as a result not just making them to look pretty.',
    twitter: 'https://twitter.com/rafaeltorales'
  },
  {
    id: shortid.generate(),
    name: 'Renato Carvalho',
    avatar:
      'https://startae.com/assets/images/team/renato-carvalho-normal_2x-05fa998c.jpg',
    role: 'Founder & CXO',
    tweet:
      'UI/UX Designer who also codes. Passionate about what he does, always looking to deliver amazing experiences.',
    twitter: 'https://twitter.com/renatolz'
  },
  {
    id: shortid.generate(),
    name: 'Victoria Haidamus',
    avatar:
      'https://startae.com/assets/images/team/victoria-haidamus-normal_2x-4a5827e7.jpg',
    role: 'Product Designer',
    tweet:
      'Designer, problem solver and passionate about learning new things. Always looking on new ways to improve how things are done.',
    twitter: 'https://twitter.com/vhaidamus'
  },
  {
    id: shortid.generate(),
    name: 'Jessica Honda',
    avatar:
      'https://startae.com/assets/images/team/jessica-honda-normal_2x-a6192e2a.jpg',
    role: 'Operations',
    tweet:
      'Service designer and longboard skate lover. Been through business consulting and digital agencies. Work with mentoring and business modeling using lenses of design.',
    twitter: 'https://twitter.com/StartaeTeam'
  },
  {
    id: shortid.generate(),
    name: 'Matheus Sales',
    avatar:
      'https://startae.com/assets/images/team/matheus-sales-normal_2x-710573b3.jpg',
    role: 'Developer',
    tweet:
      'As a Product Designer, he is always seeking to apply concept in his work and as a result not just making them to look pretty.',
    twitter: 'https://twitter.com/matheus_sales'
  },
  {
    id: shortid.generate(),
    name: 'Mario Gogh',
    avatar:
      'https://startae.com/assets/images/team/mario-gogh-normal_2x-c99911cb.jpg',
    role: 'Product Designer',
    tweet:
      'As a Product Designer, he is always seeking to apply concept in his work and as a result not just making them to look pretty.',
    twitter: 'https://twitter.com/StartaeTeam'
  },
  {
    id: shortid.generate(),
    name: 'Renato Contaifer',
    avatar:
      'https://startae.com/assets/images/team/renato-contaifer-normal_2x-205d24dc.jpg',
    role: 'Product Designer',
    tweet:
      'UI/UX Designer who also codes. Passionate about what he does, always looking to deliver amazing experiences.',
    twitter: 'https://twitter.com/StartaeTeam'
  },
  {
    id: shortid.generate(),
    name: 'Lino Gill',
    avatar:
      'https://startae.com/assets/images/team/lino-gill-normal_2x-0d85a5d0.jpg',
    role: 'Biz Dev',
    tweet:
      'UI/UX Designer who also codes. Passionate about what he does, always looking to deliver amazing experiences.',
    twitter: 'https://twitter.com/linogilll'
  },
  {
    id: shortid.generate(),
    name: 'Rafael Frota',
    avatar:
      'https://startae.com/assets/images/team/rafael-frota-normal_2x-11a1d2a1.jpg',
    role: 'Product Designer',
    tweet:
      'As a Product Designer, he is always seeking to apply concept in his work and as a result not just making them to look pretty.',
    twitter: 'https://twitter.com/StartaeTeam'
  }
]
