import { queries as TeamMemberQueries } from './team-member'

const resolvers = {
  Query: {
    ...TeamMemberQueries.resolvers
  }
}

export default resolvers
