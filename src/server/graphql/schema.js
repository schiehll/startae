import {
  schema as TeamMemberSchema,
  queries as TeamMemberQueries
} from './team-member'

const schema = `
  type Query {
    ${TeamMemberQueries.schema}
  }
`

export default [TeamMemberSchema, schema]
