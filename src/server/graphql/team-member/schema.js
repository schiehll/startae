export default `
  type TeamMember {
    id: ID
    name: String
    avatar: String
    role: String
    tweets: [String]
    twitter: String
  }
`
