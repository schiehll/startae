import team from '../../data/team'

export const schema = `
  teamMembers: [TeamMember]
`

export const resolvers = {
  teamMembers: async () => {
    return team.map(member => ({
      ...member,
      tweets: [...Array(5).keys()].map(() => member.tweet)
    }))
  }
}
